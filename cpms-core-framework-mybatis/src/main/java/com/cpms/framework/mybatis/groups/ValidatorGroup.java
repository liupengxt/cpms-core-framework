package com.cpms.framework.mybatis.groups;
import java.io.Serializable;

/**
 * @author gulang
 * @Description: 参数交易分组，根据需要可以继承此基类
 * @time: 2022/1/31 11:46
 */
public class ValidatorGroup implements Serializable {

    public  @interface Add {
    }

    public  @interface Delete {
    }

    public  @interface Select {
    }

    public  @interface Update {
    }

    public  @interface Other {
    }
}
