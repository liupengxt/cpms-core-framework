package com.cpms.framework.mybatis.utils;

import cn.hutool.core.collection.CollUtil;
import com.cpms.framework.common.core.base.BasePageVO;
import com.cpms.framework.common.utils.CsBeanUtil;

import java.util.Collections;
import java.util.List;

/**
 * @description:
 * @author: gulang
 * @time: 2022/1/26 13:55
 */
public class CsPageRespUtil {
    private CsPageRespUtil() {
    }

    /**
     * 功能描述: 对业务查询结果包装处理
     *
     * @param resultList 业务查询列表
     * @param total      数据总数
     * @param classP     业务转换的类
     */
    public static <T, P> BasePageVO<P> buildPageResult(List<T> resultList,Integer total, Class<P> classP) {
        return buildPageResult(resultList,total,null,classP);
    }

    /**
     * 功能描述: 对业务查询结果包装处理
     *
     * @param resultList 业务查询列表
     * @param total      数据总数
     * @param otherObj   附带的额外信息
     * @param classP     业务转换的类
     */
    public static <T, P> BasePageVO<P> buildPageResult(List<T> resultList,Integer total,Object otherObj, Class<P> classP) {
        BasePageVO<P> basePageVO = new BasePageVO<>();
        if (CollUtil.isNotEmpty(resultList)) {
            basePageVO.setTotal(total);
            basePageVO.setList(CsBeanUtil.copyList(resultList, classP));
            basePageVO.setOtherObj(otherObj);
        } else {
            basePageVO.setTotal(0);
            basePageVO.setList(Collections.emptyList());
            basePageVO.setOtherObj(otherObj);
        }
        return basePageVO;
    }
}
