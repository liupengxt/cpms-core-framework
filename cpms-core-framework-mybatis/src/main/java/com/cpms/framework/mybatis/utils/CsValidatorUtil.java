package com.cpms.framework.mybatis.utils;

import com.cpms.framework.common.exception.BizException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

/**
 * 类描述: 校验参数工具类，一般用在方法内调用
 * @author gulang
 * @date 2022/01/20 16:56
 */
public class CsValidatorUtil {
    private static Validator validator;

    static {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    /**
     * 校验对象
     * @param object 待校验参数对象
     * @param groups 校验分组
     */
    public static void validateModel(Object object, Class<?>... groups) throws BizException {
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(object, groups);
        if (!constraintViolations.isEmpty()) {
            throw new ConstraintViolationException(constraintViolations);
        }
    }
}
