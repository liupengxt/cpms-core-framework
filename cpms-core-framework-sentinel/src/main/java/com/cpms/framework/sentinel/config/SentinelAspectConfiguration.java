package com.cpms.framework.sentinel.config;

import com.alibaba.csp.sentinel.annotation.aspectj.SentinelResourceAspect;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author gulang
 * @Description:
 * @time: 2021/12/25 17:36
 */
@Configuration
public class SentinelAspectConfiguration {
    @Bean
    @ConditionalOnMissingBean(name = "sentinelResourceAspect")
    public SentinelResourceAspect sentinelResourceAspect() {
        return new SentinelResourceAspect();
    }
}
