package com.cpms.framework.log.handler;

import com.alibaba.fastjson.JSON;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.support.spring.PropertyPreFilters;
import com.cpms.framework.common.constants.LogTypeConstant;
import com.cpms.framework.common.core.api.Result;
import com.cpms.framework.common.utils.*;
import com.cpms.framework.log.annotations.OperLog;
import com.cpms.framework.log.dto.LogDTO;
import com.cpms.framework.log.event.LogEvent;
import com.cpms.framework.log.wrapper.RequestWrapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.Nullable;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Objects;


/**
 * @description: 请求拦截器，和过滤器不同的是拦截器可以知道当前的用户请求的是哪个控制器的哪个方法，所以功能更加强大
 * @author: gulang
 * @time: 2021/7/27 19:21
 */
public class RequestHandlerLogInterceptor implements HandlerInterceptor {
    private final Logger filterLog = LoggerFactory.getLogger(LogTypeConstant.FILTER);
    private final Logger logger = LoggerFactory.getLogger(RequestHandlerLogInterceptor.class);
    /**
     * 请求开始时间标识
     */
    private static final String LOGGER_SEND_TIME = "_send_time";
    /**
     * 请求日志实体类
     */
    private static final String LOGGER_ENTITY = "_logger_entity";
    /**
     * 日志排序字段
     */
    private static final String LOG_EXCLUDE_FIELDS = "cpms.log-exclude-fields";
    /**
     * 是否打印请求体结果
     */
    private static final String PRINT_REQUEST_RESULT = "cpms.print-request-result";

    private static final String SERVICE_NAME = "spring.application.name";

    /**
     * 请求之前处理,所有需要从request头获取信息的操作都放在这步进行，这样拿到的信息是最原始的
     * @param request
     * @param response
     * @return
     */
    @Override
    public boolean preHandle(HttpServletRequest request, @Nullable HttpServletResponse response, @Nullable Object handler) throws Exception {

        //设置请求开始时间
        request.setAttribute(LOGGER_SEND_TIME,System.currentTimeMillis());
        //获取类名
        Class<?> aClass = ((HandlerMethod) handler).getBean().getClass();
        //获取方法名
        String methodName = ((HandlerMethod) handler).getMethod().getName();
        LogDTO logDTO = new LogDTO();
        logDTO.setServiceName(CsPropsUtil.getString(SERVICE_NAME));
        logDTO.setHandleIp(CsWebUtil.getIpAddr());
        logDTO.setReqMethod(request.getMethod().toUpperCase());
        logDTO.setReqParams(getArgs(request));
        logDTO.setClassName(aClass.getSimpleName());
        logDTO.setMethodName(methodName);
        logDTO.setReqUrl(request.getRequestURI());
        request.setAttribute(LOGGER_ENTITY,logDTO);
        return  true;

    }

    /**
     * 请求方法结束之后处理
     * @param request
     * @param response
     * @param handler
     * @param ex
     */
    @Override
    @SuppressWarnings("all")
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        LogDTO logDTO = (LogDTO)request.getAttribute(LOGGER_ENTITY);
        String resultJson = String.valueOf(request.getAttribute(ResponseIntercept.RESPONSE_JSON));
        Result resultVO = JSON.parseObject(resultJson, Result.class);
        boolean ifPrint = CsPropsUtil.getBoolean(PRINT_REQUEST_RESULT);
        if(!Objects.isNull(resultVO)) {
            if(!ifPrint) {
                resultVO.setObj(null);
            }
        }
        logDTO.setResultMsg(JSON.toJSONString(resultVO));
        StringBuilder builderFilterLog = new StringBuilder();
        //结束时间
        long endTime = System.currentTimeMillis();
        //请求开始时间
        long startTime = Long.parseLong(request.getAttribute(LOGGER_SEND_TIME).toString());
        long exeTime = endTime - startTime;
        logDTO.setExeTime(exeTime);
        builderFilterLog(logDTO,builderFilterLog);
        // 操作日志记录 异步执行
        builderOperLog(handler,logDTO);
    }

    /**
     * 构建filter日志信息
     * @return
     */
    private  void builderFilterLog(LogDTO logDTO,StringBuilder builderLog){
        builderLog.append("【请求日志】-> ");
        builderLog.append("接口URI: ").append(logDTO.getReqUrl());
        builderLog.append("，请求方式 : ").append(logDTO.getReqMethod());
        builderLog.append("，类名称 : ").append(logDTO.getClassName());
        builderLog.append("，方法名称 : ").append(logDTO.getMethodName());
        builderLog.append("，入参 : ").append(logDTO.getReqParams());
        builderLog.append("，响应结果=").append(logDTO.getResultMsg());
        builderLog.append("，耗时：").append(logDTO.getExeTime()).append("ms");
        filterLog.info(builderLog.toString());
    }

    /**
     * 构建使用@OperLog注解的操作日志信息
     */
    private  void builderOperLog(Object handler,LogDTO logDTO){
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        OperLog operLog = method.getAnnotation(OperLog.class);
        if (Objects.nonNull(operLog)) {
            logDTO.setTitle(operLog.desc());
            logDTO.setCreateBy(CsSecureUtil.userAccount());
            logDTO.setTenantId(CsSecureUtil.getUser().getTenantId());
            String logExcludeFields = CsPropsUtil.getString(LOG_EXCLUDE_FIELDS);
            String reqParams = logDTO.getReqParams();
            if(CsStringUtil.isNotBlank(logDTO.getReqParams())
                    && CsStringUtil.isNotBlank(logExcludeFields)
                    && "POST".equalsIgnoreCase(logDTO.getReqMethod())
            ) {
                PropertyPreFilters.MySimplePropertyPreFilter excludefilter = new PropertyPreFilters().addFilter();
                excludefilter.addExcludes( logExcludeFields.split(","));
                try {
                    reqParams = JSONObject.toJSONString(JSONObject.parseObject(reqParams),excludefilter);
                    logDTO.setReqParams(reqParams);
                }catch (Exception e){
                    logger.error("[builderOperLog] Request Params Parse To Json Exception,reqParams={}",reqParams,e);
                }
            }
            // 发布操作日志事件，异步执行
            CsSpringUtil.publishEvent(new LogEvent(this,logDTO));
        }
    }

    /**
     * 获取请求入参
     * @param request
     * @return
     */
    private String getArgs(HttpServletRequest request){
        // 获取 url 参数
        String queryParam = request.getQueryString();
        // 获取 body json 参数
        String bodyJson  = new RequestWrapper(request).getBody();
        // !NOTE:日志打印 body参数覆盖url参数
        return StringUtils.isNotBlank(bodyJson) ? JSON.parse(bodyJson).toString() : queryParam;
    }

}
