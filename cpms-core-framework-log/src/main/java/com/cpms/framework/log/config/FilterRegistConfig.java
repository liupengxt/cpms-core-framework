package com.cpms.framework.log.config;

import com.cpms.framework.log.filter.OnceFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

/**
 * @description: 自定义filter注册
 * @author: gulang
 * @time: 2021/8/23 15:15
 */
@Configuration
public class FilterRegistConfig {

    @Bean("onceFilter")
    @SuppressWarnings("all")
    public FilterRegistrationBean OnceFilter() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(new OnceFilter());
        filterRegistrationBean.setUrlPatterns(Arrays.asList("/*"));
        filterRegistrationBean.setOrder(Integer.MIN_VALUE);
        return filterRegistrationBean;
    }
}
