package com.cpms.framework.common.constants;

/**
 * @author gulang
 * @Description:  租户相关常量
 * @time: 2021/10/3 11:49
 */
public class TenantConstant {
    /**
     * 系统总部租户编码
     */
    public static final String CPMS_HEADQUARTERS    = "CPMS_HEADQUARTERS";
    /**
     * 超级管理员编码
     */
    public static final String SUPER_ADMINISTRATOR  = "SUPER_ADMINISTRATOR";
    /**
     * 租户管理员编码
     */
    public static final String TENANT_ADMINISTRATOR = "TENANT_ADMINISTRATOR";
}
