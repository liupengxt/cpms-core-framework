package com.cpms.framework.common.enums;

/**
 * @description: 生成的随机数类型
 * @author: gulang
 * @time: 2021/9/28 10:38
 */
public enum RandomTypeEnum {
    /**
     * INT STRING ALL
     */
    INT, STRING, ALL
}
