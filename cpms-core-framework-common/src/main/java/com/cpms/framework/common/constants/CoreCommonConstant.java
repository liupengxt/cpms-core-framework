package com.cpms.framework.common.constants;

/**
 * @description:
 * @author: gulang
 * @time: 2021/9/8 18:04
 */
public class CoreCommonConstant {
    public static final String H_TOKEN_KEY = "H-Auth-Token";
    public static final String USER_INFO = "_user_info";
    public static final String CLIENT_IP_ADDR = "_client_ip_addr";
    public static final String TRACE_ID  = "traceId";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String REFRESH_TOKEN = "refresh_token";
    public static final String TOKEN_TYPE = "token_type";
    public static final String EXPIRES_IN = "expires_in";
    public static final String LICENSE = "license";
    public static final String LICENSE_NAME = "powered by cpms";
    public static final String CACHE_LOGIN_USER_INFO_KEY = "login:user:info:";
    public static final String PERMISSION_KEY = "permission";
}
