package com.cpms.framework.common.utils;

import com.cpms.framework.common.constants.CoreCommonConstant;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * @description:
 * @author: gulang
 * @time: 2021/6/7 19:01
 */
public class CsWebUtil {

    public static HttpServletRequest getRequest() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        return requestAttributes == null ? null : ((ServletRequestAttributes)requestAttributes).getRequest();
    }

    /**
     * 从header头获取TraceId
     * @return
     */
    public static String getTraceId() {
        String traceId;
        traceId = MDC.get(CoreCommonConstant.TRACE_ID);
        if(CsStringUtil.isNotBlank(traceId)) {
            return traceId;
        }
        HttpServletRequest request = getRequest();
        if(!Objects.isNull(request)) {
            traceId = request.getHeader(CoreCommonConstant.TRACE_ID);
            if(CsStringUtil.isBlank(traceId)) {
                traceId = (String)request.getAttribute(CoreCommonConstant.TRACE_ID);
            }
        }
        return traceId;
    }

    /**
     * 获取客户端IP
     * @return
     */
    public static String getClientIp() {
        HttpServletRequest request = getRequest();
        String ipAddr="";
        if(!Objects.isNull(request)) {
            ipAddr = request.getHeader(CoreCommonConstant.CLIENT_IP_ADDR);
        }
        return ipAddr;
    }

    public static String getIpAddr() {
        return getIpAddr(getRequest());
    }

    @Nullable
    public static String getIpAddr(HttpServletRequest request) {
        Assert.notNull(request, "HttpServletRequest is null");
        String ip = request.getHeader("X-Requested-For");
        if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-Forwarded-For");
        }

        if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }

        if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }

        if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }

        if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }

        if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return Objects.equals("0:0:0:0:0:0:0:1",ip) ? "127.0.0.1" : ip.split(",")[0];
    }
}
