package com.cpms.framework.common.utils;


import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.core.lookup.StrSubstitutor;

import java.util.Map;

/**
 * @description:
 * @author: gulang
 * @time: 2021/9/28 9:59
 */
public class CsStringUtil extends StringUtils {

    /**
     *  替换字符串中的${} 占位符
     * @param replaceValue
     * @param replaceTpl
     * @return
     */
    public static String strSubReplace(Map<String,String> replaceValue, String replaceTpl){
        StrSubstitutor strSubstitutor = new StrSubstitutor(replaceValue);
        return  strSubstitutor.replace(replaceTpl);
    }
}
