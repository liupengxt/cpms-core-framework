#####配置属性说明
在引入改组件的服务端模块的配置文件中，如application.yml添加如下配置
```
cpms:
   ## 是否打印请求日志输出结果，true：打印  false：不打印
   print-request-result: false

   ## 排除使用@OperLog注解记录操作日志的字段
   log-exclude-fields: password,userName

   ## 上传文件保存目录
   file-upload-path:
      win: D:/cpms/appfiles
      linux: /cpms/appfiles

   ## jwt属性配置
   jwt-secretKey: test  #jwt密钥
   jwt-token-expire: 7200000  # jwt有效时间，单位：毫秒