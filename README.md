<p align="center">
  <img src="https://img.shields.io/badge/SpringBoot-2.2.7.RELEASE-brightgreen.svg" alt="Build Status">
  <img src="https://img.shields.io/badge/JDK-11-informational.svg" alt="Coverage Status">
  <img src="https://img.shields.io/badge/mysql-5.7+-informational.svg" alt="Downloads">
  <img src="https://img.shields.io/badge/mybatis%20plus-3.4.3-yellow.svg" alt="Downloads">
  <img src="https://img.shields.io/badge/redission-3.16.0+-important.svg" alt="Downloads">
   <img src="https://img.shields.io/badge/openfeign-2.2.3.RELEASE-yellowgreen.svg" alt="Downloads">
 <img src="https://img.shields.io/badge/xxl%20job-2.3.0-blue.svg" alt="Downloads">
<img src="https://img.shields.io/badge/lombok-1.18.12-green.svg" alt="Downloads">
<img src="https://img.shields.io/badge/fastjson-1.2.75-green.svg" alt="Downloads">
<img src="https://img.shields.io/badge/guava-30.1.1%20jre-informational.svg" alt="Downloads">

<a target="_blank" href="https://gitee.com/gldcty/cpms-core-framework">
  <img src="https://img.shields.io/badge/cpms%20core%20framework-1.0.0-blue.svg" alt="Downloads">
 </a>

 <a target="_blank" href="https://www.cpms.vip">
   <img src="https://img.shields.io/badge/Copyright-@cpms-success.svg" alt="Downloads">
 </a>
 </p>  

#### 介绍
cpms-core-framework作为cpms相关项目的核心组件库，以及对第三方组件库做了进一步封装



#### 组件库说明

| 组件库                      | 描述                                                         |
| --------------------------- | ------------------------------------------------------------ |
| framework-common  | 项目公共的依赖组件，包括常用的工具类、常用第三方依赖等       |
| framework-mybatis | 使用mybatis-plus框架，添加了自动填充功能                     |
| framework-feign   | 使用openFeign框架，添加了接口调用前拦截器处理功能            |
| framework-redis   | 使用redisson框架，新增了分布式锁，防重提交注解等功能         |
| framework-secure  | 采用自定义注解+AOP实现接口权限校验                           |
| framework-log     | 1.通过拦截器记录接口请求和响应日志以及响应时间；2.监控系统操作日志，采用spring Event异步实现 |
| framework-xxl-job | 使用xxl-job框架实现分布式定时任务                            |
| framework-sentinel     | sentinel熔断限流 |
| .......                     |                                                              |




#### 相关项目

cpms-cloud微服务架构:https://gitee.com/gldcty/cpms-cloud

cpms单体架构:https://gitee.com/gldcty/cpms