package com.cpms.framework.redis.redisson;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;


/**
 * @description: redisson 配置信息
 * @author: gulang
 * @time: 2021/8/24 17:20
 */

@ConfigurationProperties(prefix = "spring.redis.server")
public class RedissonProperty {
    private String serverMode;
    private String password;
    private String clientName;
    private Single single;
    private Cluster cluster;
    private Sentinel sentinel;

    public static class  Single{
        private String address;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }
    }

    public static class  Cluster{
        private List<String> nodeHosts;

        public List<String> getNodeHosts() {
            return nodeHosts;
        }

        public void setNodeHosts(List<String> nodeHosts) {
            this.nodeHosts = nodeHosts;
        }
    }

    public static class  Sentinel{
        private String masterName;
        private List<String> nodeHosts;

        public String getMasterName() {
            return masterName;
        }

        public void setMasterName(String masterName) {
            this.masterName = masterName;
        }

        public List<String> getNodeHosts() {
            return nodeHosts;
        }

        public void setNodeHosts(List<String> nodeHosts) {
            this.nodeHosts = nodeHosts;
        }
    }

    public String getServerMode() {
        return serverMode;
    }

    public void setServerMode(String serverMode) {
        this.serverMode = serverMode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public Single getSingle() {
        return single;
    }

    public void setSingle(Single single) {
        this.single = single;
    }

    public Cluster getCluster() {
        return cluster;
    }

    public void setCluster(Cluster cluster) {
        this.cluster = cluster;
    }

    public Sentinel getSentinel() {
        return sentinel;
    }

    public void setSentinel(Sentinel sentinel) {
        this.sentinel = sentinel;
    }
}
