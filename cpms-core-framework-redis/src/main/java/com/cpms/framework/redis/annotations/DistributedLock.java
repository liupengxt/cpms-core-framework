package com.cpms.framework.redis.annotations;
import java.lang.annotation.*;

/**
 * @description: 分布式锁注解
 * @author: gulang
 * @time: 2021/11/25 16:22
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Inherited
@Documented
public @interface DistributedLock {
    /**
     * 防重复的字段，可以设置多个字段,这些字段入参中为必传且有值的字段
     * @return
     */
    String[] fields() default {};

    /**
     * key前缀固定部分，非必填
     */
    String prefixKey() default "default";

    /**
     * 没获取到锁是否直接返回，默认直接返回
     */
    boolean isBlock() default false;

    /**
     * 等待加锁时间,单位:毫秒
     */
    long waitTime() default 3000;

    /**
     * 锁过期时间，不指定的话，默认等待方法执行结束自动释放，单位：毫秒
     */
    long expireTime() default -1L;

    /**
     * 当获取锁失败时，提示语
     */
    String lockFailMsg() default "业务繁忙，请稍后再试";
}
