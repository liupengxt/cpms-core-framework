package com.cpms.framework.redis.annotations;

import java.lang.annotation.*;

/**
 * @description: 防止重复提交注解
 * @author: gualng
 * @time: 2021/11/24 14:51
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Inherited
@Documented
public @interface RepeatSubmit {
    /**
     * 过期时间，单位：毫秒，默认是5秒
     * @return
     */
    long expire() default 5000;
    /**
     * 防重复的字段，可以设置多个字段,这些字段入参中为必传且有值的字段
     * @return
     */
    String[] fields() default {};

    /**
     * key前缀固定部分，非必填
     */
    String prefixKey() default "default";

    /**
     * 提示语
     * @return
     */
    String tips() default "请勿重复操作！！！";

    /**
     * true 表示执行完方法后主动删除锁，false表示等待锁自动过期
     * @return
     */
    boolean delLock() default true;
}
