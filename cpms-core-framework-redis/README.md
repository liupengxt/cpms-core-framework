#####配置属性说明
在引入改组件的服务端模块的配置文件中，如application.yml添加如下配置
```
spring:
    redis:
        lettuce:
            # Redis 线程池设置
            pool:
                #连接池最大连接数（使用负值表示没有限制） 默认 8
                max-active: 8
                #连接池中的最大空闲连接 默认 8
                max-idle: 8
                #连接池最大阻塞等待时间（使用负值表示没有限制） 默认 -1
                max-wait: -1
                #连接池中的最小空闲连接 默认 0
                min-idle: 0
        #服务配置
        server:
            server-mode: "single" # single(单机模式)、cluster（集群模式）、sentinel（哨兵模式）
            clientName:  # 客户端名称
            password:  #密码，没有可不填
            single:
                address: 127.0.0.1:6379
            cluster:
                node-hosts:
                    - Ma1873Gq-1.cachesit.xxx:8080
                    - Ma1873Gq-1.cachesit.xxx:8080
                    - Ma1873Gq-1.cachesit.xxx:8080
                    - Ma1873Gq-1.cachesit.xxx:8080
            sentinel:
                master-name: redis-sentinel-master
                node-hosts:
                    - Ma1873Gq-1.cachesit.xxx:8080
                    - Ma1873Gq-1.cachesit.xxx:8080
                    - Ma1873Gq-1.cachesit.xxx:8080
